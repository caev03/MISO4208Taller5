describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEvent(10);
    })
})
function randomClick(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if(!Cypress.Dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({force: true});
                monkeysLeft = monkeysLeft - 1;
            }
            setTimeout(randomClick, 1000, monkeysLeft);
        });
    }   
}

function randomEvent(monkeysLeft){
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    if(monkeysLeft > 0) {
        var event = getRandomInt(1,4);
        switch(event){
            case 1:
                cy.get('a').then($links => {
                    var randomLink = $links.get(getRandomInt(0, $links.length));
                    if(!Cypress.Dom.isHidden(randomLink)) {
                        cy.wrap(randomLink).click({force: true});
                        monkeysLeft = monkeysLeft - 1;
                    }
                    setTimeout(randomClick, 1000, monkeysLeft);
                });
                break;
            case 2:
                cy.get('input').then($inputs => {
                    var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
                    if(!Cypress.Dom.isHidden(randomInput)) {
                        cy.wrap(randomInput).click({force: true}).type("random");
                        monkeysLeft = monkeysLeft - 1;
                    }
                    setTimeout(randomInput, 1000, monkeysLeft);
                });
                break;
            case 3:
                cy.get('select').then($selects => {
                    var randomSelect = $selects.get(getRandomInt(0, $selects.length));
                    var options = randomSelect.options
                    var randomOption = options[getRandomInt(0,options.length)]
                    if(!Cypress.Dom.isHidden(randomOption)) {
                        cy.wrap(randomSelect).select(randomOption.value);
                        monkeysLeft = monkeysLeft - 1;
                    }
                    setTimeout(randomSelect, 1000, monkeysLeft);
                });
                break;
            default:
                cy.get('button').then($buttons => {
                    var randomButton = $buttons.get(getRandomInt(0, $buttons.length));
                    if(!Cypress.Dom.isHidden(randomButton)) {
                        cy.wrap(randomButton).click({force: true}).type("random");
                        monkeysLeft = monkeysLeft - 1;
                    }
                    setTimeout(randomButton, 1000, monkeysLeft);
                });
                break;
        }
    }
}